import { writable } from 'svelte/store';
import { easings } from '$lib/HelperFunctions.js';
import { Projects } from '$lib/Projects.js';

export const STATE = {
  Home:       "home",       // IF the home screen is shown
  Contact:    "contact",    // If the contact grid is shown
  About:      "about",      // If the about page is shown
  Grid:       "grid",       // If the projects grid is shown
  Project:    "project",    // If a project is shown
  Loading:    "loading",    // If the page is loading
  MobileMenu: "mobileMenu", // If the hamburger menu is shown (mobile)
};

export const lenis = writable();
export const currentScrollEasing = writable(easings.easeOutQuint);

export const shownProject = writable('none');

export const state = writable(STATE.Loading);

export const canScroll = writable(true);

export const projects = writable(Projects);