const loremIpsum = "I annat så blivit sorgliga bäckasiner blev omfångsrik, stora kunde både del ingalunda annan söka, blev nya är kanske av om. Rännil mot erfarenheter annan både sällan inom av jäst det smultron, år miljoner av brunsås vi denna både om hans blev händer, samtidigt det enligt stora dimma som trevnadens det och. Sjö lax hela händer bra äng ännu vi både från, där sjö är söka stora som gör av dunge, kan lax dunge ännu och precis björnbär åker.";

export const Projects = [
  {
    projectName: "ANU", 
    id: "anu", 
    type: "Branding/Graphic Design",
    overview: loremIpsum,
    tools: ["Blender", "Fusion 360", "Photoshop", "Premiere Pro", "After Effects"],
    creativeFields: ["Product Design", "Web Design", "Industrial Design", "Branding", "3D Design & Motion"],
    tags: ["Design", "Electronics", "Keyboard", "Rendering"],
    preview_mp4: new URL(`./img/projects/anu/anim.mov`, import.meta.url).href, // TODO: Change this to `preview_mov` across all projects.
    preview_webm: new URL(`./img/projects/anu/anim.webm`, import.meta.url).href,
    thumb_mp4: new URL(`./img/projects/anu/preview.mp4`, import.meta.url).href
  },
  {
    projectName: "INANNA-N60",
    id: "inanna-n60", 
    type: "Product Design",
    overview: loremIpsum,
    tools: ["Blender", "Fusion 360", "Photoshop", "Premiere Pro", "After Effects"],
    creativeFields: ["Product Design", "Industrial Design", "3D Design & Motion"],
    tags: ["Design", "Electronics", "Keyboard", "Rendering"],
    preview_mp4: new URL(`./img/projects/album-covers/anim.mov`, import.meta.url).href,
    preview_webm: new URL(`./img/projects/album-covers/anim.webm`, import.meta.url).href
  },
  {
    projectName: "ENZU",
    id: "enzu", 
    type: "Branding/Graphic Design",
    overview: loremIpsum,
    tools: ["Blender", "Fusion 360", "Photoshop", "Premiere Pro", "After Effects"],
    creativeFields: ["Product Design", "Web Design", "Industrial Design", "Branding", "3D Design & Motion"],
    tags: ["Design", "Electronics", "Keyboard", "Rendering"],
    preview_mp4: new URL(`./img/projects/enzu/anim.mov`, import.meta.url).href,
    preview_webm: new URL(`./img/projects/enzu/anim.webm`, import.meta.url).href,
    thumb_mp4: new URL(`./img/projects/enzu/preview.mp4`, import.meta.url).href
  },
  {
    projectName: "ARC90", 
    id: "arc90", 
    type: "Architecture Design",
    overview: loremIpsum,
    tools: ["Blender", "Fusion 360", "Photoshop", "Premiere Pro", "After Effects"],
    creativeFields: ["Product Design", "Industrial Design", "3D Design & Motion"],
    tags: ["Design", "Electronics", "Keyboard", "Rendering"],
    preview_mp4: new URL(`./img/projects/arc90/anim.mov`, import.meta.url).href,
    preview_webm: new URL(`./img/projects/arc90/anim.webm`, import.meta.url).href
  },
  {
    projectName: "ALBUM COVERS", 
    id: "album-covers", 
    type: "Graphic Design",
    overview: loremIpsum,
    tools: ["Blender", "Fusion 360", "Photoshop", "Premiere Pro", "After Effects"],
    creativeFields: ["Product Design", "Industrial Design", "3D Design & Motion"],
    tags: ["Design", "Electronics", "Keyboard", "Rendering"],
    preview_mp4: new URL(`./img/projects/album-covers/anim.mov`, import.meta.url).href,
    preview_webm: new URL(`./img/projects/album-covers/anim.webm`, import.meta.url).href,
    thumb_mp4: new URL(`./img/projects/album-covers/preview.mp4`, import.meta.url).href
  },
  {
    projectName: "LOGOFOLIO", 
    id: "logofolio", 
    type: "Graphic Design",
    overview: loremIpsum,
    tools: ["Blender", "Fusion 360", "Photoshop", "Premiere Pro", "After Effects"],
    creativeFields: ["Product Design", "Industrial Design", "Branding", "3D Design & Motion"],
    tags: ["Design", "Electronics", "Keyboard", "Rendering"],
    preview_mp4: new URL(`./img/projects/logofolio/anim.mov`, import.meta.url).href,
    preview_webm: new URL(`./img/projects/logofolio/anim.webm`, import.meta.url).href,
    thumb_mp4: new URL(`./img/projects/logofolio/preview.mp4`, import.meta.url).href
  },
  {
    projectName: "NATURAL HABITAT", 
    id: "natural-habitat", 
    type: "Graphic Design",
    overview: loremIpsum,
    tools: ["Blender", "Fusion 360", "Photoshop", "Premiere Pro", "After Effects"],
    creativeFields: ["Product Design", "Industrial Design", "3D Design & Motion"],
    tags: ["Design", "Electronics", "Keyboard", "Rendering"],
    preview_mp4: new URL(`./img/projects/natural-habitat/anim.mov`, import.meta.url).href,
    preview_webm: new URL(`./img/projects/natural-habitat/anim.webm`, import.meta.url).href
  },
  {
    projectName: "TRE", 
    id: "tre", 
    type: "Graphic Design",
    overview: loremIpsum,
    tools: ["Blender", "Fusion 360", "Photoshop", "Premiere Pro", "After Effects"],
    creativeFields: ["Product Design", "Industrial Design", "3D Design & Motion"],
    tags: ["Design", "Electronics", "Keyboard", "Rendering"],
    preview_mp4: new URL(`./img/projects/tre/anim.mov`, import.meta.url).href,
    preview_webm: new URL(`./img/projects/tre/anim.webm`, import.meta.url).href,
    thumb_mp4: new URL(`./img/projects/tre/preview.mp4`, import.meta.url).href
  }
];

export default Projects;