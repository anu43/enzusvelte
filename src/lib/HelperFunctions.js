export const ucCharacterSet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
export const lcCharacterSet = 'abcdefghijklmnopqrstuwxyz';

export function browserCanPlayH265(vidEl) {
  return vidEl.canPlayType('video/mp4; codecs=hvc1') === "probably";
}

export function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1) + min); // The maximum is inclusive and the minimum is inclusive
}

export function getRandomFloat(min, max) {
  return Math.random() * (max - min) + min; // The maximum is inclusive and the minimum is inclusive
}

export function mapRange(in_min, in_max, out_min, out_max) {
  return (this - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

export function getRandomChar(lowerCase) {
  return lowerCase ? lcCharacterSet[Math.floor(Math.random() * lcCharacterSet.length)] : ucCharacterSet[Math.floor(Math.random() * ucCharacterSet.length)];
}

export function getRandomString(numChars) {
  let randomStr = '';
  for (let i = 0; i < numChars; i++)
  {
    randomStr += getRandomChar();
  }
  return randomStr;
}

export function shuffleArray(array) {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
  return array;
}

export function importAll(r) {
  let files = {};
  r.keys().map(item => { files[item.replace('./', '')] = r(item); });
  return files;
}

export function importProjectMedia(project) {
  const projectId = project.id;

  let mediaFiles = {};

  switch (projectId) {
    case 'anu':
      mediaFiles = import.meta.glob('./img/projects/anu/*.(mp4|webm|webp)', {query: "?url", eager: true});
      break;
    case 'inanna-n60':
      mediaFiles = import.meta.glob('./img/projects/inanna-n60/*.(mp4|webm|webp)', {query: "?url", eager: true});
      break;
    case 'enzu':
      mediaFiles = import.meta.glob('./img/projects/enzu/*.(mp4|webm|webp)', {query: "?url", eager: true});
      break;
    case 'arc90':
      mediaFiles = import.meta.glob('./img/projects/arc90/*.(mp4|webm|webp)', {query: "?url", eager: true});
      break;
    case 'album-covers':
      mediaFiles = import.meta.glob('./img/projects/album-covers/*.(mp4|webm|webp)', {query: "?url", eager: true});
      break;
    case 'logofolio':
      mediaFiles = import.meta.glob('./img/projects/logofolio/*.(mp4|webm|webp)', {query: "?url", eager: true});
      break;
    case 'natural-habitat':
      mediaFiles = import.meta.glob('./img/projects/natural-habitat/*.(mp4|webm|webp)', {query: "?url", eager: true});
      break;
    case 'tre':
      mediaFiles = import.meta.glob('./img/projects/tre/*.(mp4|webm|webp)', {query: "?url", eager: true});
      break;
    default:
      mediaFiles = import.meta.glob('./img/projects/*.(mp4|webm|webp)', {query: "?url", eager: true});
      break;
  }

  let fileArr = [];

  Object.entries(mediaFiles).forEach(element => {
    // element[0] is the file path (e.g. '../img/projects/album-covers/1.webp').
    // element[1].default is the file url (e.g. 'https://localhost:4173/img/projects/album-covers/1.Wodo3DI15.webp').
    if ( !element[0].includes('anim.mp4') 
      && !element[0].includes('anim.webm') 
      && !element[0].includes('anim.mov') 
      && !element[0].includes('preview.mp4'))
      fileArr.push({name: element[0], url: element[1].default});
  });
  
  // Sort function to sort files by the prefix in the filename being an integer from 0 to n (base 10).
  fileArr.sort((a, b) => {
    const getPrefixNumber = (filename) => {
      return filename ? parseInt(filename.split('/').pop().split('.')[0], 10) : 0;
    };
    return getPrefixNumber(a.url) - getPrefixNumber(b.url);
  });
  
  return fileArr.map(file => file.url);
}

export function removeClass(query, className) {
  document.querySelectorAll(query).forEach((element) => {
    if (element.classList.contains(className))
      element.classList.remove(className);
  });
}

export function addClass(query, className) {
  document.querySelectorAll(query).forEach((element) => {
    if (!element.classList.contains(className))
      element.classList.add(className);
  });
}

export function hasClass(query, className) {
  return Array.from(document.querySelectorAll(query)).some(({classList}) => classList.contains(className));
}

export function elHasClass(el, className) {
  return el.classList.contains(className);
}

export const elementIsInViewport = (el, partiallyVisible = false) => {
  const { top, left, bottom, right } = el.getBoundingClientRect();
  const { innerHeight, innerWidth } = window;
  return partiallyVisible
    ? ((top > 0 && top < innerHeight) ||
        (bottom > 0 && bottom < innerHeight)) &&
        ((left > 0 && left < innerWidth) || (right > 0 && right < innerWidth))
    : top >= 0 && left >= 0 && bottom <= innerHeight && right <= innerWidth;
};

export function getKeyByValue(object, value) {
  return Object.keys(object).find(key => object[key] === value);
}

export function shuffleCharacters(el, text) {
  const numIterations = text.length;
  const duration = 100; // Duration of each iteration in milliseconds
  const delay = 50; // Delay between iterations in milliseconds
  let i = 0;
  var interval = setInterval(() => {
    if (i < numIterations)
    {
      el.textContent = el.textContent.charAt(el.textContent.length - 1) + el.textContent.substring(0, el.textContent.length - 1);
      i++;
    }
    else
    {
      clearInterval(interval);
    }
  }, delay);
}

export function shuffleRandomChars(el, text, lowerCase, iterations = 2) {

    const duration = 25; // Duration of each iteration in milliseconds
    const delay = 12; // Delay between iterations in milliseconds

    const chars = text.split('');
    const originalText = [...chars]; // Make a copy of the original text

    const shuffleIntervals = [];

    el.textContent = getRandomString();

    for (let i = 0; i < chars.length; i++) {
      let numIterations = iterations;
      shuffleIntervals.push(
        setInterval(() => {
          if (numIterations > 0) {
            if (lowerCase)
              chars[i] = getRandomChar(true); // Shuffle the character (lowercase)
            else
              chars[i] = getRandomChar(false); // Shuffle the character (uppercase)
            el.textContent = chars.join(''); // Update the element's content with the new character
            numIterations--;
          } else {
            chars[i] = originalText[i]; // Restore the original character
            el.textContent = chars.join(''); // Update the element's content with the original character
            clearInterval(shuffleIntervals[i]); // Clear the interval for this character
          }
        }, duration + i * delay) // Add a delay between intervals for different characters
      );
    }
}

export async function getCachedUrl(url) {
  const cache = await caches.open('resource-cache');
  const cachedResponse = await cache.match(url);
  if (cachedResponse) {
    const objectURL = URL.createObjectURL(await cachedResponse.blob());
    url = objectURL;
  }
  
  return url;
}

export const easings = {
  // easeOutSine:    {name: 'easeOutSine', thumb: '/img/easings/easeOutSine.svg', func: (t) => { return Math.sin((t * Math.PI) / 2) }},
  // easeOutCubic:   {name: 'easeOutCubic', thumb: '/img/easings/easeOutCubic.svg', func: (t) => { return 1 - Math.pow(1 - t, 3) }},
  easeOutQuint:   {name: 'easeOutQuint', thumb: '/img/easings/easeOutQuint.svg', func: (t) => { return 1 - Math.pow(1 - t, 5) }}
  // easeOutCirc:    {name: 'easeOutCirc', thumb: '/img/easings/easeOutCirc.svg', func: (t) => { return Math.sqrt(1 - Math.pow(t - 1, 2)) }},
  // easeOutElastic: {name: 'easeOutElastic', thumb: '/img/easings/easeOutElastic.svg', func: (t) => {
  //   const c4 = (2 * Math.PI) / 3;
  //   return t === 0
  //   ? 0
  //   : t === 1
  //   ? 1
  //   : Math.pow(2, -20 * t) * Math.sin((t * 5 - 0.7) * c4) + 1;
  // }},
  // easeOutQuad:    {name: 'easeOutQuad', thumb: '/img/easings/easeOutQuad.svg', func: (t) => { return 1 - Math.pow(1 - t, 2) }},
  // easeOutQuart:   {name: 'easeOutQuart', thumb: '/img/easings/easeOutQuart.svg', func: (t) => { return 1 - Math.pow(1 - t, 4) }},
  // easeOutExpo:    {name: 'easeOutExpo', thumb: '/img/easings/easeOutExpo.svg', func: (t) => { return t === 1 ? 1 : 1 - Math.pow(2, -10 * t) }},
  // easeOutBack:    {name: 'easeOutBack', thumb: '/img/easings/easeOutBack.svg', func: (t) => { const c1 = 1.70158; const c3 = c1 + 1; return 1 + c3 * Math.pow(t - 1, 3) + c1 * Math.pow(t - 1, 2); }},
  // easeOutBounce:  {name: 'easeOutBounce', thumb: '/img/easings/easeOutBounce.svg', func: (t) => { const n1 = 7.5625; const d1 = 2.75;
  //   if (t < 1 / d1)
  //     return n1 * t * t;
  //   else if (t < 2 / d1) 
  //     return n1 * (t -= 1.5 / d1) * t + 0.75;
  //   else if (t < 2.5 / d1) 
  //     return n1 * (t -= 2.25 / d1) * t + 0.9375;
  //   else
  //     return n1 * (t -= 2.625 / d1) * t + 0.984375;
  // }},
  // easeOutTen:     {name: 'easeOutTen', thumb: '', func: (t) => { return 1.4 - Math.pow(1 - t, 14) }}
}