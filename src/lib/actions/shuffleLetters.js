import shuffleLettersLib from 'shuffle-letters';

export function shuffleLetters(node, params) {
    shuffleLettersLib(node, params);

    return {
        update(newParams) {
            shuffleLettersLib(node, newParams);
        }
    };
} 