self.onmessage = async (event) => {
  const { resourceUrls } = event.data;
  const totalResources = resourceUrls.length;
  let loadedResources = 0;
  let totalSize = 0; // Initialize total size

  const startTime = performance.now(); // Record the start time

  const updateProgress = () => {
    const progress = loadedResources / totalResources;
    self.postMessage({ progress });
  };

  const cache = await caches.open('resource-cache');

  for (const url of resourceUrls) {
    try {
      const cachedResponse = await cache.match(url);
      if (!cachedResponse) {
        const response = await fetch(url);
        const contentLength = response.headers.get('Content-Length');
        if (contentLength) {
          totalSize += parseInt(contentLength, 10);
        }
        await cache.put(url, response.clone());
      }
      loadedResources++;
      updateProgress();
    } catch (error) {
      console.error('Error loading resource:', error);
    }
  }

  const endTime = performance.now(); // Record the end time
  const totalTime = endTime - startTime; // Calculate the total loading time
  // console.debug("Loaded ", loadedResources, "/", totalResources, " resources");
  // console.debug("Total loading time: ", totalTime, "ms");
  const totalSizeMB = totalSize / (1024 * 1024); // Convert total size to megabytes
  // console.debug("Total size of all resources: ", totalSizeMB.toFixed(2), "MB");
  self.postMessage({ progress: 1, done: true, totalTime, totalSize: totalSizeMB });
};
